<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tareas</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-ms-12">
                <h1>Tareas</h1>
                <hr>           
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-ms-6">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Título</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Fecha</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                include("controller_list.php");
                                while($row = $result->fetch_assoc()){
                            ?>
                            <tr>
                                <td><?php echo $row["titulo"]?></td>
                                <td><?php echo $row["descripcion"]?></td>
                                <td><?php echo $row["fecha"]?></td>
                                <td>
                                    <a type="button" class="btn btn-info btn-sm" href="index.php?id=<?php echo $row["id"] ?>">Editar</a>
                                </td>
                                <td>
                                    <a type="button" class="btn btn-danger btn-sm" href="controller_delete.php?id=<?php echo $row["id"] ?>">Eliminar</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>          
            </div>
            <div class="col-lg-6 col-ms-6">
                <?php
                    IF( isset( $_GET['id'] ) ){
                        include("controller_find_by_id.php");
                        while($row = $result->fetch_assoc()){
                            echo 
                            '<form action="controller_update.php" method="post">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="id_control" value= "'. $row["id"] .'">
                                </div>
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" class="form-control" name="titulo_control" value= "'. $row["titulo"] .'">
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <textarea class="form-control" name="descripcion_control" rows="3">'. $row["descripcion"] .'</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Fecha</label>
                                    <input type="date" class="form-control" name="fecha_control" value= "'. $row["fecha"] .'">
                                </div>
                                <button type="submit" name="submit" class="btn btn-primary">Actualizar</button>
                            </form> ';
                        }
                    } ELSE {
                        echo 
                        '<form action="controller_save.php" method="post">
                            <div class="form-group">
                                <label>Título</label>
                                <input type="text" class="form-control" name="titulo_control">
                            </div>
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea class="form-control" name="descripcion_control" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Fecha</label>
                                <input type="date" class="form-control" name="fecha_control">
                            </div>
                            <button type="submit" name="submit" class="btn btn-primary">Guardar</button>
                        </form>';
                    }
                ?>
            </div>
         </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
